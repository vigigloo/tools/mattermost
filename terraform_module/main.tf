resource "helm_release" "mattermost" {
  chart           = "mattermost-team-edition"
  repository      = "https://gitlab.com/api/v4/projects/30792282/packages/helm/stable"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = concat([
    file("${path.module}/mattermost.yaml"),
  ], var.values)

  dynamic "set" {
    for_each = var.image_repository == null ? [] : [var.image_repository]
    content {
      name  = "image.repository"
      value = var.image_repository
    }
  }

  dynamic "set" {
    for_each = var.image_tag == null ? [] : [var.image_tag]
    content {
      name  = "image.tag"
      value = var.image_tag
    }
  }

  dynamic "set" {
    for_each = var.mattermost_externaldb_connection_string == null ? [] : [var.mattermost_externaldb_connection_string]
    content {
      name  = "externalDB.enabled"
      value = "true"
    }
  }

  dynamic "set" {
    for_each = var.mattermost_externaldb == null ? [] : [var.mattermost_externaldb]
    content {
      name  = "externalDB.externalDriverType"
      value = var.mattermost_externaldb
    }
  }

  dynamic "set_sensitive" {
    for_each = var.mattermost_externaldb_connection_string == null ? [] : [var.mattermost_externaldb_connection_string]
    content {
      name  = "externalDB.externalConnectionString"
      value = var.mattermost_externaldb_connection_string
    }
  }

  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "resources.limits.cpu"
      value = var.limits_cpu
    }
  }

  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "resources.limits.memory"
      value = var.limits_memory
    }
  }

  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "resources.requests.cpu"
      value = var.requests_cpu
    }
  }

  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "resources.requests.memory"
      value = var.requests_memory
    }
  }

  dynamic "set" {
    for_each = var.ingress_host == null ? [] : [var.ingress_host]
    content {
      name  = "ingress.host"
      value = var.ingress_host
    }
  }

  dynamic "set" {
    for_each = var.mattermost_configJSON_ServiceSettings_SiteURL == null ? [] : [var.mattermost_configJSON_ServiceSettings_SiteURL]
    content {
      name  = "configJSON.ServiceSettings.SiteURL"
      value = var.mattermost_configJSON_ServiceSettings_SiteURL
    }
  }
}
