variable "mattermost_externaldb" {
  type = string
}

variable "mattermost_externaldb_connection_string" {
  type = string
}

variable "mattermost_configJSON_ServiceSettings_SiteURL" {
  type    = string
  default = null
}